#coding:utf-8

import os
import torch

from torch import nn
from munch import Munch
from transforms import build_transforms
from Data import util

import torch.nn.functional as F
import numpy as np
from collections import defaultdict

def compute_d_loss(nets, args, x_real, y_org, y_trg, z_trg=None, x_ref=None, use_r1_reg=True, use_adv_cls=False, use_con_reg=False, epoch_num=-1, batch_num=-1, debug_batch=False, disc_train_type=None, logger=None, num_batches=-1):
    args = Munch(args)

    # ensures only one of the two is empty
    assert (z_trg is None) != (x_ref is None)

    # real/fake discriminator with real audios

    # TODO: check output of discrim here for a trained model to check

    # real/fake discriminator output = P(real), regardless of domain (across all domains/domain independent)
    x_real.requires_grad_()
    # well trained D will output 1 (P(real))
    out = nets.discriminator(x_real, y_org)  # y_org just used for indexing, x is the only proper input to D
    loss_real = adv_loss(out, 1)

    # TODO: for debugging
    # a dict of dicts (i.e. a nested dict) that has the stats of the outputs of the two discriminators on the last batch of the epoch only asthe values of each leaf node dict
    # organised by network (realfake, src_classif) and training type (random reference or target reference)
    nwks_out_stats = {}
    if debug_batch:
        # get statistics of batch mel spec data
        stats_dict = util.batch_discrim_stats(out)
        logger.info(f"Ep{epoch_num}bch{batch_num}_{disc_train_type} r/f D out stats: max_val={stats_dict['max_val']:.2f}, min_val={stats_dict['min_val']:.2f}, var={stats_dict['var']:.2f}")
        # for the last batch of epoch only
        if batch_num == num_batches:
            nwks_out_stats[f'debug/{disc_train_type}/realfake'] = {
                'max': stats_dict['max_val'],
                'min':stats_dict['min_val'], 
                'var':stats_dict['var']
            }
    
    # R1 regularizaition (https://arxiv.org/abs/1801.04406v4)
    # not mentioned in paper
    if use_r1_reg:
        loss_reg = r1_reg(out, x_real)
    else:
        loss_reg = torch.FloatTensor([0]).to(x_real.device)
    
    # consistency regularization (bCR-GAN: https://arxiv.org/abs/2002.04724)
    # not mentioned in paper
    loss_con_reg = torch.FloatTensor([0]).to(x_real.device)
    if use_con_reg:
        t = build_transforms()
        out_aug = nets.discriminator(t(x_real).detach(), y_org)
        loss_con_reg += F.smooth_l1_loss(out, out_aug)
    
    # real/fake discriminator with fake audios

    with torch.no_grad():
        if z_trg is not None:
            s_trg = nets.mapping_network(z_trg, y_trg)
        else:  # x_ref is not None
            s_trg = nets.style_encoder(x_ref, y_trg)
            
        F0 = nets.f0_model.get_feature_GAN(x_real)
        x_fake = nets.generator(x_real, s_trg, masks=None, F0=F0)
    # well trained D will output 0 (P(real))
    out = nets.discriminator(x_fake, y_trg)     # y_trg just used for indexing, x is the only proper input to D
    loss_fake = adv_loss(out, 0)
    if use_con_reg:
        out_aug = nets.discriminator(t(x_fake).detach(), y_trg)
        loss_con_reg += F.smooth_l1_loss(out, out_aug)
    
    # adversarial source classifier loss
    if use_adv_cls:
        out_de = nets.discriminator.classifier(x_fake)
        # when training C we want C to output y_src as the source domain
        bool_arr = y_org != y_trg
        if torch.any(bool_arr): # bool_arr not all Falses
            out_de_mismatch = out_de[bool_arr]
            y_org_mismatch = y_org[bool_arr]
            # only check entropy between classifier predictions and y_org vals for which C did not correctly predict y
            loss_real_adv_cls = F.cross_entropy(out_de_mismatch, y_org_mismatch)
        else: # both args to CE would be [] (perfect classifier) resulting in loss_real_adv_cls = nan 
            loss_real_adv_cls = torch.zeros(1).mean()
            # only use logger if this function is called in _train_epoch(), not for _eval_epoch() as logger will be None
            if disc_train_type:
                logger.info(f"Ep{epoch_num}bch{batch_num} discrim training: loss_real_adv_cls would be = nan")


        # TODO: for debugging
        if debug_batch:
            # get statistics of batch mel spec data
            stats_dict = util.batch_discrim_stats(out)
            logger.info(f"Ep{epoch_num}bch{batch_num}_{disc_train_type} src classif D out stats: max_val={stats_dict['max_val']:.2f}, min_val={stats_dict['min_val']:.2f}, var={stats_dict['var']:.2f}")
            # for the last batch of epoch only
            if batch_num == num_batches:
                nwks_out_stats[f'debug/{disc_train_type}/src_classif'] = {
                    'max': stats_dict['max_val'],
                    'min': stats_dict['min_val'], 
                    'var': stats_dict['var']
                }
        # not mentioned in paper
        if use_con_reg:
            out_de_aug = nets.discriminator.classifier(t(x_fake).detach())
            loss_con_reg += F.smooth_l1_loss(out_de, out_de_aug)
    else:
        loss_real_adv_cls = torch.zeros(1).mean()
        # when source classifier is not used yet, add 0s for stats
        if batch_num == num_batches:
                nwks_out_stats[f'debug/{disc_train_type}/src_classif'] = {'max': 0.0, 'min': 0.0, 'var': 0.0}
    
    # loss_real + loss_fake = L_adv in paper
    loss = loss_real + loss_fake + args.lambda_reg * loss_reg + \
            args.lambda_adv_cls * loss_real_adv_cls + \
            args.lambda_con_reg * loss_con_reg 

    return loss, Munch(real=loss_real.item(),
                       fake=loss_fake.item(),
                       reg=loss_reg.item(),
                       real_adv_cls=loss_real_adv_cls.item(),
                       con_reg=loss_con_reg.item()), nwks_out_stats

def compute_g_loss(nets, args, x_real, y_org, y_trg, z_trgs=None, x_refs=None, use_adv_cls=False, epoch_num=-1, batch_num=-1, debug_batch=False, gen_train_type=None, logger=None, num_batches=-1):
    args = Munch(args)
    # ensures only one of the two is empty
    assert (z_trgs is None) != (x_refs is None)
    if z_trgs is not None:
        z_trg, z_trg2 = z_trgs
    if x_refs is not None:
        x_ref, x_ref2 = x_refs
        
    # compute style vectors
    if z_trgs is not None:
        s_trg = nets.mapping_network(z_trg, y_trg)
    else:
        s_trg = nets.style_encoder(x_ref, y_trg)
    
    # compute ASR/F0 features (real)
    with torch.no_grad():
        F0_real, GAN_F0_real, cyc_F0_real = nets.f0_model(x_real)
        ASR_real = nets.asr_model.get_feature(x_real)
    
    # adversarial loss
    x_fake = nets.generator(x_real, s_trg, masks=None, F0=GAN_F0_real)
    # a good discriminator will output -> 0 for a really fake input (i.e. G makes bad/unrealistic output)
    # we want to improve G to generate more realistic fake data, 'tricking' D into outputting -> 1
    # this will make loss_adv smaller
    out = nets.discriminator(x_fake, y_trg)
    loss_adv = adv_loss(out, 1)

    # TODO: for debugging
    # a dict of dicts (i.e. a nested dict) that has the stats of the output of the generator on the last batch of the epoch only as the values of the leaf node dict
    # organised by network (g) and training type (random reference or target reference)
    nwks_out_stats = {}
    if debug_batch:
        # get statistics of batch mel spec data
        stats_dict = util.batch_spec_stats(x_fake)
        logger.info(f"Ep{epoch_num}bch{batch_num}_{gen_train_type} G out stats: max_val={stats_dict['max_val']:.2f}, min_val={stats_dict['min_val']:.2f}, mean_variance={stats_dict['mean_var']:.2f}")
        # 0th elem in batch as numpy mx
        util.plot_spectrogram(
            torch.squeeze(x_fake[0]).cpu().detach().numpy(),
            epoch_num=epoch_num, batch_num=batch_num, gen_train_type=gen_train_type, batch_stats=stats_dict, logger=logger
        )
        # for the last batch of epoch only
        if batch_num == num_batches:
            nwks_out_stats[f'debug/{gen_train_type}/g'] = {
                'max': stats_dict['max_val'],
                'min':stats_dict['min_val'], 
                'var':stats_dict['mean_var']
            }
    
    # compute ASR/F0 features (fake)
    F0_fake, GAN_F0_fake, _ = nets.f0_model(x_fake)
    ASR_fake = nets.asr_model.get_feature(x_fake)
    
    # norm consistency loss
    x_fake_norm = log_norm(x_fake)
    x_real_norm = log_norm(x_real)
    loss_norm = ((torch.nn.ReLU()(torch.abs(x_fake_norm - x_real_norm) - args.norm_bias))**2).mean()
    
    # F0 loss
    loss_f0 = f0_loss(F0_fake, F0_real)
    
    # style F0 loss (style initialization)
    if x_refs is not None and args.lambda_f0_sty > 0 and not use_adv_cls:
        F0_sty, _, _ = nets.f0_model(x_ref)
        loss_f0_sty = F.l1_loss(compute_mean_f0(F0_fake), compute_mean_f0(F0_sty))
    else:
        loss_f0_sty = torch.zeros(1).mean()
    
    # ASR loss
    loss_asr = F.smooth_l1_loss(ASR_fake, ASR_real)
    
    # style reconstruction loss
    s_pred = nets.style_encoder(x_fake, y_trg)
    loss_sty = torch.mean(torch.abs(s_pred - s_trg))
    
    # diversity sensitive loss
    if z_trgs is not None:
        s_trg2 = nets.mapping_network(z_trg2, y_trg)
    else:
        s_trg2 = nets.style_encoder(x_ref2, y_trg)
    x_fake2 = nets.generator(x_real, s_trg2, masks=None, F0=GAN_F0_real)
    x_fake2 = x_fake2.detach()
    _, GAN_F0_fake2, _ = nets.f0_model(x_fake2)
    loss_ds = torch.mean(torch.abs(x_fake - x_fake2))
    loss_ds += F.smooth_l1_loss(GAN_F0_fake, GAN_F0_fake2.detach())
    
    # cycle-consistency loss
    s_org = nets.style_encoder(x_real, y_org)
    x_rec = nets.generator(x_fake, s_org, masks=None, F0=GAN_F0_fake)
    loss_cyc = torch.mean(torch.abs(x_rec - x_real))
    # F0 loss in cycle-consistency loss
    if args.lambda_f0 > 0:
        _, _, cyc_F0_rec = nets.f0_model(x_rec)
        loss_cyc += F.smooth_l1_loss(cyc_F0_rec, cyc_F0_real)
    if args.lambda_asr > 0:
        ASR_recon = nets.asr_model.get_feature(x_rec)
        loss_cyc += F.smooth_l1_loss(ASR_recon, ASR_real)
    
    if use_adv_cls:
        out_de = nets.discriminator.classifier(x_fake)
        # when training G we want C to output y_trg as the source domain
        bool_arr = y_org != y_trg
        if torch.any(bool_arr): # bool_arr not all Falses
            out_de_mismatch = out_de[bool_arr]
            y_trg_mismatch = y_trg[bool_arr]
            # only check entropy between classifier predictions and y_trg vals for which C did not correctly predict y
            loss_adv_cls = F.cross_entropy(out_de_mismatch, y_trg_mismatch)
        else: # both args to CE would be [] (perfect classifier) resulting in loss_real_adv_cls = nan 
            loss_adv_cls = torch.zeros(1).mean()
            # only use logger if this function is called in _train_epoch(), not for _eval_epoch() as logger will be None
            if gen_train_type:
                logger.info(f"Ep{epoch_num}bch{batch_num} generator training: loss_adv_cls would be = nan")
    else:
        loss_adv_cls = torch.zeros(1).mean()
    
    loss = args.lambda_adv * loss_adv + args.lambda_sty * loss_sty \
           - args.lambda_ds * loss_ds + args.lambda_cyc * loss_cyc\
           + args.lambda_norm * loss_norm \
           + args.lambda_asr * loss_asr \
           + args.lambda_f0 * loss_f0 \
           + args.lambda_f0_sty * loss_f0_sty \
           + args.lambda_adv_cls * loss_adv_cls

    c = loss_adv.item()

    return loss, Munch(adv=loss_adv.item(),
                       sty=loss_sty.item(),
                       ds=loss_ds.item(),
                       cyc=loss_cyc.item(),
                       norm=loss_norm.item(),
                       asr=loss_asr.item(),
                       f0=loss_f0.item(),
                       adv_cls=loss_adv_cls.item()), nwks_out_stats
    
# for norm consistency loss
def log_norm(x, mean=-4, std=4, dim=2):
    """
    normalized log mel -> mel -> norm -> log(norm)
    """
    x = torch.log(torch.exp(x * std + mean).norm(dim=dim))
    return x

# for adversarial loss
def adv_loss(logits, target):
    assert target in [1, 0]
    if len(logits.shape) > 1:
        logits = logits.reshape(-1)
    targets = torch.full_like(logits, fill_value=target)
    # TODO: nan checked here!
    logits = logits.clamp(min=-10, max=10) # prevent nan
    loss = F.binary_cross_entropy_with_logits(logits, targets)
    return loss

# for R1 regularization loss
def r1_reg(d_out, x_in):
    # zero-centered gradient penalty for real images
    batch_size = x_in.size(0)
    grad_dout = torch.autograd.grad(
        outputs=d_out.sum(), inputs=x_in,
        create_graph=True, retain_graph=True, only_inputs=True
    )[0]
    grad_dout2 = grad_dout.pow(2)
    assert(grad_dout2.size() == x_in.size())
    reg = 0.5 * grad_dout2.view(batch_size, -1).sum(1).mean(0)
    return reg

# for F0 consistency loss
def compute_mean_f0(f0):
    # calculate mean for each batch using matrix input f0 that contains the n batches
    # batch is a row, so mean calculated row-wise, collapsing the last dimension (column)
    f0_mean = torch.tensor(f0.mean(-1), device=torch.device('cuda:0'))
    # expand mean value for each batch by pasting it the same number of times as there are entries in f0 for the batch
    # creates a column vector for each batch, with the mean value pasted
    # transpose from column vectors to row vectors
    if bool(f0_mean.shape):
        # batch size > 1
        f0_mean = f0_mean.expand(f0.shape[-1], f0_mean.shape[0]).transpose(0, 1) # (B, M)
    else:
        # batch size = 1
        # collapse the row dimension (the dim that has value of 1) to have just a list, so that shape has 1 value, matching f0 input
        # squeeze (collapse) required in case batch size is 1
        f0_mean = f0_mean.expand(f0.shape[-1], 1).transpose(0, 1).squeeze(0) # (B, M)
    return f0_mean

def f0_loss(x_f0, y_f0):
    """
    x.shape = (B, 1, M, L): predict
    y.shape = (B, 1, M, L): target
    """
    # compute the mean
    x_mean = compute_mean_f0(x_f0)
    y_mean = compute_mean_f0(y_f0)
    loss = F.l1_loss(x_f0 / x_mean, y_f0 / y_mean)
    return loss