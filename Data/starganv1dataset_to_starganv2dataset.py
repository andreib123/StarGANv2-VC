import os
import shutil


if __name__ == "__main__":

    in_dir = "/dev/shm/andrei/datasets/stargan_training_dataset1"
    out_dir = "/dev/shm/andrei/datasets/stargan_training_dataset2"

    new_spkr_num = 1
    # var per speaker in in_dir
    cmu_spkr_num = -1
    pfs_spkr_num = -1
    myst_spkr_num = -1
    ljs_spkr_num = -1

    cur_speaker_folder = os.path.join(out_dir, f"speaker{new_spkr_num}")
    # loop through all speaker subdirectories on source corpus (assume corpus is prepared for StarGAN training)
    for root, dirs, files in os.walk(in_dir, topdown=False):
        # only process speaker subdirectories
        if root and not dirs:
            # assume all filenames in a folder follow the same naming convention
            if "cmukids" in files[0]:
                files.sort()
                for file in files:
                    if file.endswith(".wav"):
                        # if new speaker number
                        spkr_num = int(file.split('_')[0].split("cmukids")[-1])
                        if spkr_num != cmu_spkr_num:
                            # create new output speaker
                            new_spkr_folder = os.path.join(out_dir, f"speaker{new_spkr_num}")
                            if not os.path.exists(new_spkr_folder): os.makedirs(new_spkr_folder, exist_ok=True)
                            # set current output speaker folder to the newly created folder
                            cur_speaker_folder = new_spkr_folder
                            cmu_spkr_num = spkr_num
                            new_spkr_num += 1
                        # copy file to current output speaker folder
                        shutil.copyfile(os.path.join(root, file), os.path.join(cur_speaker_folder, file))
            elif "pfstar" in files[0]:
                files.sort()
                for file in files:
                    if file.endswith(".wav"):
                        # if new speaker number
                        spkr_num = int(file.split('_')[0].split("pfstar")[-1])
                        if spkr_num != pfs_spkr_num:
                            # create new output speaker
                            new_spkr_folder = os.path.join(out_dir, f"speaker{new_spkr_num}")
                            if not os.path.exists(new_spkr_folder): os.makedirs(new_spkr_folder, exist_ok=True)
                            # set current output speaker folder to the newly created folder
                            cur_speaker_folder = new_spkr_folder
                            pfs_spkr_num = spkr_num
                            new_spkr_num += 1
                        # copy file to current output speaker folder
                        shutil.copyfile(os.path.join(root, file), os.path.join(cur_speaker_folder, file))
            elif "myst" in files[0]:
                files.sort()
                for file in files:
                    if file.endswith(".wav"):
                        # if new speaker number
                        spkr_num = int(file.split('_')[0].split("myst")[-1])
                        if spkr_num != myst_spkr_num:
                            # create new output speaker
                            new_spkr_folder = os.path.join(out_dir, f"speaker{new_spkr_num}")
                            if not os.path.exists(new_spkr_folder): os.makedirs(new_spkr_folder, exist_ok=True)
                            # set current output speaker folder to the newly created folder
                            cur_speaker_folder = new_spkr_folder
                            myst_spkr_num = spkr_num
                            new_spkr_num += 1
                        # copy file to current output speaker folder
                        shutil.copyfile(os.path.join(root, file), os.path.join(cur_speaker_folder, file))
            elif "ljspeech" in files[0]:
                files.sort()
                for file in files:
                    if file.endswith(".wav"):
                        # if new speaker number
                        spkr_num = int(file.split('_')[0].split("ljspeech")[-1])
                        if spkr_num != ljs_spkr_num:
                            # create new output speaker
                            new_spkr_folder = os.path.join(out_dir, f"speaker{new_spkr_num}")
                            if not os.path.exists(new_spkr_folder): os.makedirs(new_spkr_folder, exist_ok=True)
                            # set current output speaker folder to the newly created folder
                            cur_speaker_folder = new_spkr_folder
                            ljs_spkr_num = spkr_num
                            new_spkr_num += 1
                        # copy file to current output speaker folder
                        shutil.copyfile(os.path.join(root, file), os.path.join(cur_speaker_folder, file))

    # rename folders from just "speaker" to their source corpus names. 
    # loop through all leaf subdirectories on source corpus (assuming all audio files in corpus located in leaf dirs)
    for root, dirs, files in os.walk(out_dir, topdown=False):
        if not dirs:
            rename_to = files[0].split('/')[-1].split('_')[0]
            rename_to = os.path.join('/'.join(root.split('/')[:-1]), rename_to)
            os.rename(root, os.path.join(rename_to))
