import argparse
from util import setup_logging, get_dataset_time_stats, plot_dataset_stats_histogram


if __name__ == "__main__":
    # parse program arguments
    parser = argparse.ArgumentParser(
        description="Generate a postproc_stats.log text file in the specified prepared dataset directory describing statistics on the audiofiles within.")
    parser.add_argument("--path", type=str, default=None, required=True,
                        help="path to the dataset")
    args = parser.parse_args()

    setup_logging(path=args.path, log_file_name="postproc_stats")

    f, t = get_dataset_time_stats(args.path)
    # create and save the stats in a histogram image.
    plot_dataset_stats_histogram(args.path, f, t)
    