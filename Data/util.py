from collections import defaultdict
from email.policy import default
import os
import logging
import sys
import math
import random
import logging
import librosa.display
import matplotlib.pyplot as plt
import yaml
import statistics
from pydub import AudioSegment
from collections import Counter


def setup_logging(path=None, log_file_name=None):
    """Set up logging to both console and a logfile."""
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)  # track INFO logging events (default was WARNING)
    root_logger.handlers = []  # clear handlers
    root_logger.addHandler(logging.StreamHandler(sys.stdout))  # handler to log to console
    root_logger.addHandler(logging.FileHandler(f"{path}/{log_file_name}.log"))  # handler to log to file also
    root_logger.handlers[0].setFormatter(logging.Formatter("%(levelname)s: %(message)s"))  # log level and message
    root_logger.handlers[1].setFormatter(logging.Formatter("%(levelname)s: %(message)s"))


def to_hms(s):
    # allow overflow in the hours part if >24, e.g. 51:59:59, instead of 03:59:59 problem
    m, s = divmod(s, 60)
    h, m = divmod(m, 60)
    return '{:0>2}:{:0>2}:{:0>2}'.format(h, m, s)


def files_batch(files, batch_size):
    """A generator function that outputs a smaller list, which is a subset of the files in a folder.
    
    Useful when iterating over the files in a folder where the number of files is extremely large.

    Args:
      files (list str):
        A list of all the filenames in a folder.
      batch_size (int):
        The length of the smaller yielded lists.

    Yields:
      A list that is batch_size in length.
      The last list yielded may be smaller in length if batch_size doesn't divide evenly into files length.
    """
    # get number of batches that can be created from the files list using batch_size number of files per batch
    # last batch may be smaller than batch_size
    files_len = len(files)
    num_batches = math.ceil(files_len/batch_size) # ceil ensures num_batches is at least 1
    num_batches_cnt, k = 0, 0
    # yield each batch
    while num_batches_cnt < num_batches:
        yield files[k:k+batch_size] if k+batch_size < files_len else files[k:]
        k+=batch_size
        num_batches_cnt+=1


def combine(root_dir, file_names) -> AudioSegment:
    """Combines all audio files in a directory into one object.

    Args:
      root_dir (str):
        The path to the directory containing the files.
      file_names (list):
        The audio filenames to combine from that directory.

    Returns:
      A concatenated pydub.AudioSegment audio object.
    """
    audio = AudioSegment.empty()
    for filename in file_names:
        if filename.endswith('.wav'):
            f = os.path.join(root_dir, filename)
            audio += AudioSegment.from_wav(f)
    return audio


def create_train_test_split(path=None):
    """creates and saves text files indicating the audio files from the (anyspeaker) prepared dataset used for training and testing.
    
    Args:
      path (str):
        Path to the dataset folder containing speaker folders and to where the text files are saved to.
    """
    # initialise list of dict objects where each object is composed of:
    # {'Path'    = <path/to/audio/file> (str)
    #  'Speaker' = <ID>                 (int)
    # }
    data_list = []

    # folder names do not have to contain speaker IDs, they can have any unique name.
    speakers = [name for name in os.listdir(path) if os.path.isdir(os.path.join(path, name))]
    speakers.sort()

    # dict of speaker names (folder names) and corresponding IDs
    speaker_names_ids_dict = {}

    logging.info("Generating train/test split info...")
    # loop over the speaker leaf folders
    for path, subdirs, files in os.walk(path, topdown=False):
        for name in files:
            if name.endswith(".wav"):
                # get the leaf folder name
                speaker = path.split('/')[-1]
                if speaker in speakers:
                    # in the output txt file, the first speaker in speakers list will have an ID of 0 etc.
                    data_list.append({"Path": os.path.join(path, name), "Speaker": speakers.index(speaker)})
                    speaker_names_ids_dict[speaker] = speakers.index(speaker)

    # list of tuples of (speaker ID, speaker name) sorted in ascending ID order.
    speaker_ids_names = sorted((v,k) for k,v in speaker_names_ids_dict.items())
    random.shuffle(data_list)
    # 90% train : 10% test split
    split_idx = round(len(data_list) * 0.1)

    test_data = data_list[:split_idx]
    train_data = data_list[split_idx:]

    # write text files
    
    # entries in the text files are as follows:
    # /path/to/<output_dataset_name>/<speaker_folder>/<file_name>.wav|<speaker ID>

    # create train_list.txt
    file_str = ""
    for d in train_data:
        file_str += d['Path'] + "|" +str(d['Speaker'])+ '\n'
    with open(path + "/train_list.txt", "w") as f:
        f.write(file_str)
    logging.info(f"Audio files' paths used for training saved to: {path}/train_list.txt")

    # create val_list.txt
    file_str = ""
    for d in test_data:
        file_str += d['Path'] + "|" + str(d['Speaker']) + '\n'
    with open(path + "/val_list.txt", "w") as f:
        f.write(file_str)
    logging.info(f"Audio files' paths used for validation saved to: {path}/val_list.txt")
    logging.info("Done.")

    # create speaker_ids.txt
    with open(path + "/speaker_ids.txt", 'w') as f:
        for id, name in speaker_ids_names:
            f.write(f"{name}|{id}\n")


def batch_spec_stats(mel_batch):
    d = dict()  # dict of the stats in batch
    l = list()  # list of variances of mel specs in batch
    d['max_val'] = mel_batch.max().item()
    d['min_val'] = mel_batch.min().item()
    mel_batch = mel_batch.squeeze()
    # loop through all mel specs in batch
    for mel_tensor_idx in range(mel_batch.shape[0]):
        # add variance of mel spec to list
        l.append(mel_batch[mel_tensor_idx, :, :].var().item())
    d['mean_var'] = statistics.fmean(l)
    return d


def batch_discrim_stats(discrim_out_batch):
    d = dict()  # dict of the stats in batch
    d['max_val'] = discrim_out_batch.max().item()
    d['min_val'] = discrim_out_batch.min().item()
    d['var'] = discrim_out_batch.var().item()
    return d


def get_dataset_time_stats(path):
    """Generate and plot audio time stats from the prepared dataset.
    
    Args:
      path (str):
        Path to which to save the created histogram.
      frequency_table (collections.Counter):
        A frequency table of the lengths of audio files rounded to the nearest second and the number of such occurrences.
      total_time (float):
        The total audio length of the prepared dataset.
    """
    freqs = list()
    total_time = 0
    # loop through all speaker subdirectories on source corpus (assume corpus is prepared for StarGAN training)
    for root, dirs, files in os.walk(path, topdown=False):
        # only process speaker subdirectories
        if root and not dirs:
            speaker_length = 0.0 # total length of speaker audio in seconds
            # get the speaker subdirectory name
            name = root.split('/')[-1]
            # remove any non-wav files
            wavs = list(filter(lambda x: x.endswith('.wav'), files))
            batch_size = 150
            # loop through all the wav audio files in the speaker subdirectory in batches
            for batch in files_batch(wavs, batch_size):
                for wav in batch:
                    full_wav_path = os.path.join(root, wav)
                    # open the wav
                    audio = AudioSegment.from_wav(full_wav_path)
                    # get audio file length information
                    l = math.floor(audio.duration_seconds)
                    speaker_length += l
                    freqs.append(l)
            logging.info(f"Total length of audio for the speaker '{name}' is {to_hms(int(speaker_length))}s")
            total_time += speaker_length
    freq_table = Counter(freqs)

    return freq_table, total_time


def plot_dataset_stats_histogram(path, frequency_table, total_time):
    """Generate and plot audio time stats from the prepared dataset.
    
    Args:
      path (str):
        Path to which to save the created histogram.
      frequency_table (collections.Counter):
        A frequency table of the lengths of audio files rounded to the nearest second and the number of such occurrences.
      total_time (float):
        The total audio length of the prepared dataset.
    """
    key_sorted_table = sorted(frequency_table.items())
    for l,f in key_sorted_table:
        logging.info(f"Num audiofiles with length {l} seconds: {f}")
    mode_element = frequency_table.most_common()[0]
    mode_len = mode_element[0]
    mode_freq = mode_element[1]
    logging.info(f"Most common audio length: {mode_len} seconds, with {mode_freq} occurrences.")
    logging.info("Total audio length of the dataset: " + to_hms(int(total_time)) + "s")
    logging.info("Creating histogram of audio length frequencies...")
    # create plot of the distribution of prepared audio file lengths
    #plt.bar(frequency_table.keys(), frequency_table.values())
    fig, ax = plt.subplots()
    bars = ax.bar(frequency_table.keys(), frequency_table.values())
    # diplay the y values over the bars
    ax.bar_label(bars)
    #cur_y = plt.gca().get_yticks()
    #cur_x = plt.gca().get_xticks()
    #new_x = range(math.floor(min(cur_x)), math.ceil(max(cur_x)))
    #new_y = range(math.floor(min(cur_y)), math.ceil(max(cur_y)), 10)
    # show only the x values present in the frequency table on the x axis as ticks
    new_x = sorted(list(frequency_table.keys()))
    #new_y = sorted(list(frequency_table.values()))
    plt.xticks(new_x)
    #plt.yticks(new_y)
    # hide y axis ticks
    plt.yticks([])
    plt.title("Prepared Dataset Audio File Lengths")
    plt.xlabel("File length (rounded to nearest second)")
    plt.ylabel("Frequency")
    #plt.show()
    plt.savefig(f"{path}/histogram.png")
    logging.info(f"Histogram saved to {path}/histogram.png")
    logging.info("Done.")


def plot_spectrogram(spec, epoch_num=-1, batch_num=-1, gen_train_type=None, batch_stats=None, logger=None):
    if spec is not None:
        try:
            fig = plt.Figure()
            axs = fig.add_subplot(111)
            axs.set_title(f"Log Mel Spec (db): epoch {epoch_num}, batch {batch_num}, elem 0")
            p = librosa.display.specshow(spec, ax=axs, y_axis='log', x_axis='time')
            fig.colorbar(p, ax=axs)
            axs.set_ylabel('Hz (mel)')
            axs.set_xlabel('Time', loc="left")

            if batch_stats:
                axs.text(0.3, -0.11,
                f"Batch stats: max_val={batch_stats['max_val']:.2f}, min_val={batch_stats['min_val']:.2f}, mean_variance={batch_stats['mean_var']:.2f}",
                wrap=True, horizontalalignment='left', transform=axs.transAxes, size=9)
        

            # TODO: bad hardcoded practice, assumes original training config file specified to train.py is always at that path
            config = yaml.safe_load(open('Configs/config.yml'))
            log_dir = config['log_dir']
            out_dir = log_dir+'/generator_out_specs'
            if not os.path.exists(out_dir): os.makedirs(out_dir, exist_ok=True)

            fig.savefig(out_dir + f"/epoch{epoch_num}_batch{batch_num}_{gen_train_type}_spec.png")
        except IndexError as e:
            if logger:
                logger.error(f"Can't create epoch{epoch_num}_batch{batch_num}_{gen_train_type}_spec.png, caught following exception:")
                logger.error(e)