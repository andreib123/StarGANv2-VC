import argparse
from util import create_train_test_split


if __name__ == "__main__":
    # parse program arguments
    parser = argparse.ArgumentParser(
        description="Create the text files indicating the train/test splits for a multispeaker dataset.")
    parser.add_argument("--path", type=str, default=None, required=True,
                        help="path to the dataset")
    args = parser.parse_args()

    create_train_test_split(path=args.path)