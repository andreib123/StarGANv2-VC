import argparse
import os
from pydub import AudioSegment
import math

if __name__ == "__main__":
    # parse program arguments
    parser = argparse.ArgumentParser(
        description="Remove any wav files that are longer than length specified.")
    parser.add_argument("--path", type=str, default=None, required=True,
                        help="path to the dataset")
    parser.add_argument("--length", type=int, default=None, required=True,
                        help="audio files longer than or equal to this length in seconds will be removed from the dataset.")
    args = parser.parse_args()

    # loop through all speaker subdirectories on source corpus (assume corpus is prepared for StarGAN training)
    for root, dirs, files in os.walk(args.path, topdown=False):
        # only process speaker subdirectories
        if root and not dirs:
            # remove any non-wav files
            wavs = list(filter(lambda x: x.endswith('.wav'), files))
            # loop through all the wav audio files in the speaker subdirectory
            for wav in wavs:
                full_wav_path = os.path.join(root, wav)
                # open the wav
                audio = AudioSegment.from_wav(full_wav_path)
                # get audio file length information
                l = math.floor(audio.duration_seconds)
                # remove audio file if length is more than or equal to the length argument
                if l >= args.length:
                    os.remove(full_wav_path)