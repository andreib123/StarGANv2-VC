import os
import argparse

def create_speakers_names_ids_file(path):
    path_train = os.path.join(path, "train_list.txt")
    path_test = os.path.join(path, "val_list.txt")

    filepaths = [path_train, path_test]

    lines = list()
    for fp in filepaths:
        with open(fp, "r") as f:
            for line in f:
                lines.append(line.rstrip('\n'))

    # dict of speaker names (folder names) and corresponding IDs
    speaker_names_ids_dict = {}
    for line in lines:
        spkr, id = line.split('|')[0].split('/')[-1].split('.wav')[0].split('_')[0], line.split('|')[-1]
        speaker_names_ids_dict[spkr] = int(id)

    # list of tuples of (speaker ID, speaker name) sorted in ascending ID order.
    speaker_ids_names = sorted((v,k) for k,v in speaker_names_ids_dict.items())
    with open(os.path.join(path, "speaker_ids.txt"), 'w') as f:
        for id, name in speaker_ids_names:
            f.write(f"{name}|{id}\n")


if __name__ == "__main__":
    # parse program arguments
    parser = argparse.ArgumentParser(
        description="Create a text file listing the speaker names and corresponding IDs for a multispeaker dataset AFTER train_list.txt and val_list.txt files are created (i.e. after Data/create_train_test_split.py was called.")
    parser.add_argument("--path", type=str, default=None, required=True,
                        help="path to the dataset")
    args = parser.parse_args()

    create_speakers_names_ids_file(path=args.path)