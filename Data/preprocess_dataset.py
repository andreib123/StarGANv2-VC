
# script to preprocess any dataset (multi or single, using .wav only) into format suitable for training StarGANv2-VC model

import os
import time
import logging
import argparse
from collections import Counter
from util import combine, files_batch, to_hms, setup_logging, create_train_test_split, plot_dataset_stats_histogram
import matplotlib.pyplot as plt
from pydub.silence import split_on_silence



def get_chunks_stats(chunks):
    # total length of audio for this list of chunks in seconds
    total_time = sum(map(lambda x: x.duration_seconds, chunks))
    # frequency table rounded to the nearest integer for the lengths of the chunks in seconds
    freq_table = Counter(map(lambda x: int(x.duration_seconds), chunks))
    return (total_time, freq_table)


def split(sound):
    """Splits a pydub.AudioSegment into multiple objects by the silence between utterances.

    Args:
      sound (pydub.AudioSegment):
        The audio to split by silence.

    Returns:
      A list of pydub.AudioSegment audio objects.
    """
    dBFS = sound.dBFS
    chunks = split_on_silence(sound,
        min_silence_len = 100,
        silence_thresh = dBFS-16,
        keep_silence = 100
    )
    return chunks


def save_chunks(chunks, dst_dir, spkr_folder_name, single_speaker=True):
    """Prepares audio chunks optimally for training and saves prepared audio examples in an output directory as one speaker (by default) or multiple speakers.

    The inputted list of audio chunks are usually very short in length (less than 1 sec) due to being divided by silence.
    They are concatenated into longer audio chunks that have a target length.
    These are then resampled to a target sampling frequency.
    They are then saved under the speaker name as a subfolder.

    Args:
      chunks (list of pydub.AudioSegment elements).
      dst_dir (str):
        The path to the directory containing the speaker directories.
      spkr_folder_name (int):
        The folder name under which to save the chunks in the output dataset.
      single_speaker (bool):
        Flag used to specify whether the prepared audio chunks will be saved under a single speaker folder.

    Returns (float, collections.Counter):
      A tuple where the first element is the total length of audio from the output chunks
      and the second element is a frequency table for the lengths of the chunks rounded to the nearest second.

      Using an integer rounded Counter object saves memory as otherwise a very large Counter object or list would
      be accumulated when the whole source dataset is processed if using exact float values.
    """
    if single_speaker:
        speaker_path = os.path.join(dst_dir, "speaker1")
        if not os.path.exists(speaker_path):
            os.makedirs(speaker_path, exist_ok=True)
    else:
        speaker_path = os.path.join(dst_dir, spkr_folder_name)
    

    target_length = 5 * 1000  # preferred length of audio in ms for StarGAN training
    output_chunks = [chunks[0]]  # extract first AudioSegment object from chunks list and initialise new list
    # loop through remaining AudioSegment objects
    for chunk in chunks[1:]:
        if len(output_chunks[-1]) < target_length:  # latest chunk in output list is shorter than target length
            # concatenate audio chunks into new larger chunk
            output_chunks[-1] += chunk
        else:
            # if the last output chunk is longer than the target length, we can start a new one
            output_chunks.append(chunk)
    
    # do not use chunks with lengths less than 1 second
    output_chunks_non_zero = list(filter(lambda x: int(x.duration_seconds) > 0, output_chunks))

    # resample audio to 24kHz
    for j, chunk in enumerate(output_chunks_non_zero):
        chunk = chunk.set_frame_rate(24000)
        chunk = chunk.set_channels(1)
        # save resampled chunk to file
        tag = args.out_tag if args.out_tag else ''
        chunk.export(os.path.join(speaker_path, tag + '_' + str(j) + '.wav'), format="wav")


    return get_chunks_stats(output_chunks_non_zero)


def process_corpus(multispeaker=False):
    """Prepares a source dataset with multiple speakers into a single speaker (by default) or multispeaker (keeping the speakers the same) output dataset for StarGAN training."""
    # accumulated audio time stats in the prepared output corpus, later sent to stats function
    total_time = 0.0
    freqs = Counter()
    # loop through all leaf subdirectories on source corpus (assuming all audio files in corpus located in leaf dirs)
    for i, v in enumerate(os.walk(args.source, topdown=False)):
            root, dirs, files = v
            if not dirs: # if no more subdirectories
                batch_size = 150
                for batch in files_batch(files, batch_size):
                    # prepare chunks and save them under one child speaker identity
                    audio = combine(root, batch)
                    chunks  = split(audio)
                    if multispeaker:
                        t, f = save_chunks(chunks, args.output, root.split('/')[-1], single_speaker=False)
                    else:
                        t, f = save_chunks(chunks, args.output)
                    # update stats
                    total_time += t
                    freqs.update(f)
    logging.info("Finished processing!")
    logging.info("Processed output dataset statistics:")
    # get stats from freq table, note: all stats are rounded to the 'seconds' granularity
    plot_dataset_stats_histogram(freqs, total_time)


if __name__ == "__main__":
    # parse program arguments
    parser = argparse.ArgumentParser(
        description="Preprocess any multi or single speaker dataset, where .wav audio files are in leaf subdirs, to an output dataset for training. Output dataset can be single speaker (default mode - combines source speakers into one) or multispeaker (keeping number of speakers the same from source to output).")
    parser.add_argument("--source", type=str, default=None, required=True,
                        help="path to the source dataset to preprocess")
    parser.add_argument("--output", type=str, default=None, required=True,
                        help="path to the newly created prepared output dataset")
    parser.add_argument("--out_tag", type=str, default=None, required=False,
                        help="string tag included in every audio file name in the output dataset, useful when mixing datasets")
    parser.add_argument("--multispeaker", type=bool, default=False, required=False, action='store_true',
                        help="Flag used to specify whether the output dataset will be multispeaker. Defaults to False if flag is not given.")
    args = parser.parse_args()

    # start timing how long it takes to prepare data
    tic = time.perf_counter()

    # create required destination corpus dir
    os.makedirs(args.output, exist_ok=True)

    # log to both console and logfile
    setup_logging(path=args.output, log_file_name="preproc_stats")
    logging.info(f"Source dataset to process: {args.source}")
    logging.info(f"Saving prepared/processed data to output dataset: {args.output}")
    logging.info(f"Saving log of dataset preparation run to: {args.output}/stats.log")
    logging.info(f"Processing started...")

    # create prepared output dataset for training StarGAN model
    process_corpus(multispeaker=args.multispeaker)
    
    # create text files indicating train/test splits of the prepared dataset that will be read by the training script
    create_train_test_split(path=args.output)

    toc = time.perf_counter()
    logging.info(f"Finished data preparation in {time.strftime('%H:%M:%Ss', time.gmtime(toc - tic))}")
