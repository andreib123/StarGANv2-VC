# populate original directory with extra wav version of each sph file in that directory

import os
from sphfile import SPHFile


path = '/home/andrei/datasets/cmu_kids'  # Path of folder containing .sph files

# loop through all leaf subdirectories (assuming all audio files in corpus located in leaf dirs)
for root, dirs, files in os.walk(path, topdown=False):
    if not dirs: # if no more subdirectories
        for filename in files:
            if filename.endswith('.sph'):
                full_f_path = os.path.join(root, filename)
                sph = SPHFile(full_f_path)
                f_name_no_ext = filename.replace('.sph', '')
                out_file_path = root + '/' + f_name_no_ext + '.wav'
                sph.write_wav(out_file_path)