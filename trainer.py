# -*- coding: utf-8 -*-

import os
import os.path as osp
import sys
import time
from collections import defaultdict

import numpy as np
import torch
from torch import nn
from PIL import Image
from tqdm import tqdm

from losses import compute_d_loss, compute_g_loss

class Trainer(object):
    def __init__(self,
                 args,
                 model=None,
                 model_ema=None,
                 optimizer=None,
                 scheduler=None,
                 config={},
                 device=torch.device("cpu"),
                 loggers=None,
                 train_dataloader=None,
                 val_dataloader=None,
                 initial_steps=0,
                 initial_epochs=0,
                 fp16_run=False
    ):
        self.args = args
        self.steps = initial_steps
        self.epochs = initial_epochs
        self.model = model
        self.model_ema = model_ema
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.train_dataloader = train_dataloader
        self.val_dataloader = val_dataloader
        self.config = config
        self.device = device
        self.finish_train = False
        self.loggers = loggers # dict of loggers with keys 'logger_train' and 'logger_debug'
        self.fp16_run = fp16_run

    def _train_epoch(self):
        """Train model one epoch."""
        raise NotImplementedError

    @torch.no_grad()
    def _eval_epoch(self):
        """Evaluate model one epoch."""
        pass

    def save_checkpoint(self, checkpoint_path):
        """Save checkpoint.
        Args:
            checkpoint_path (str): Checkpoint path to be saved.
        """
        state_dict = {
            "optimizer": self.optimizer.state_dict(),
            "steps": self.steps,
            "epochs": self.epochs,
            "model": {key: self.model[key].state_dict() for key in self.model}
        }
        if self.model_ema is not None:
            state_dict['model_ema'] = {key: self.model_ema[key].state_dict() for key in self.model_ema}

        if not os.path.exists(os.path.dirname(checkpoint_path)):
            os.makedirs(os.path.dirname(checkpoint_path))
        torch.save(state_dict, checkpoint_path)

    def load_checkpoint(self, checkpoint_path, load_only_params=False):
        """Load checkpoint.

        Args:
            checkpoint_path (str): Checkpoint path to be loaded.
            load_only_params (bool): Whether to load only model parameters.

        """
        state_dict = torch.load(checkpoint_path, map_location="cpu")
        for key in self.model:
            self._load(state_dict["model"][key], self.model[key])

        if self.model_ema is not None:
            for key in self.model_ema:
                self._load(state_dict["model_ema"][key], self.model_ema[key])
        
        if not load_only_params:
            self.steps = state_dict["steps"]
            self.epochs = state_dict["epochs"]
            self.optimizer.load_state_dict(state_dict["optimizer"])


    def _load(self, states, model, force_load=True):
        model_states = model.state_dict()
        for key, val in states.items():
            try:
                if key not in model_states:
                    continue
                if isinstance(val, nn.Parameter):
                    val = val.data

                if val.shape != model_states[key].shape:
                    self.loggers['logger_train'].info("%s does not have same shape" % key)
                    print(val.shape, model_states[key].shape)
                    if not force_load:
                        continue

                    min_shape = np.minimum(np.array(val.shape), np.array(model_states[key].shape))
                    slices = [slice(0, min_index) for min_index in min_shape]
                    model_states[key][slices].copy_(val[slices])
                else:
                    model_states[key].copy_(val)
            except:
                self.loggers['logger_train'].info("not exist :%s" % key)
                print("not exist ", key)

    @staticmethod
    def get_gradient_norm(model):
        total_norm = 0
        for p in model.parameters():
            param_norm = p.grad.data.norm(2)
            total_norm += param_norm.item() ** 2

        total_norm = np.sqrt(total_norm)
        return total_norm

    @staticmethod
    def length_to_mask(lengths):
        mask = torch.arange(lengths.max()).unsqueeze(0).expand(lengths.shape[0], -1).type_as(lengths)
        mask = torch.gt(mask+1, lengths.unsqueeze(1))
        return mask

    def _get_lr(self):
        for param_group in self.optimizer.param_groups:
            lr = param_group['lr']
            break
        return lr

    @staticmethod
    def moving_average(model, model_test, beta=0.999):
        for param, param_test in zip(model.parameters(), model_test.parameters()):
            param_test.data = torch.lerp(param.data, param_test.data, beta)

    def _train_epoch(self):
        self.epochs += 1

        train_losses = defaultdict(list)
        # list of nested dictionaries, contains the stats (min, max, variance) of the outputs of the networks, which are plotted in tensorboard
        nwks_out_stats = [] 

        _ = [self.model[k].train() for k in self.model]
        scaler = torch.cuda.amp.GradScaler() if (('cuda' in str(self.device)) and self.fp16_run) else None

        use_con_reg = (self.epochs >= self.args.con_reg_epoch)
        use_adv_cls = (self.epochs >= self.args.adv_cls_epoch)

        # total number of batches in the training dataset
        num_batches = len(self.train_dataloader)
        
        for train_steps_per_epoch, batch in enumerate(tqdm(self.train_dataloader, desc="[train]"), 1):

            ### load data
            batch = [b.to(self.device) for b in batch]
            x_real, y_org, x_ref, x_ref2, y_trg, z_trg, z_trg2 = batch

            # TODO: this is for debugging the output of the discriminators and generator
            # debug the outputs only on the middle and last batch of each epoch to save time and memory
            debug_batch = True if (train_steps_per_epoch == int(num_batches/2) or train_steps_per_epoch==num_batches) else False

            # discriminator training
            
            # train the discriminator (by random reference)
            disc_train_type = "randref"

            self.optimizer.zero_grad()
            if scaler is not None:
                with torch.cuda.amp.autocast():
                    d_loss, d_losses_latent, stats = compute_d_loss(self.model, self.args.d_loss, x_real, y_org, y_trg, z_trg=z_trg, use_adv_cls=use_adv_cls, use_con_reg=use_con_reg,
                        epoch_num=self.epochs, batch_num=train_steps_per_epoch, debug_batch=debug_batch, disc_train_type=disc_train_type, logger=self.loggers['logger_debug'], num_batches=num_batches
                    )
                scaler.scale(d_loss).backward()
            else:
                d_loss, d_losses_latent, stats = compute_d_loss(self.model, self.args.d_loss, x_real, y_org, y_trg, z_trg=z_trg, use_adv_cls=use_adv_cls, use_con_reg=use_con_reg,
                    epoch_num=self.epochs, batch_num=train_steps_per_epoch, debug_batch=debug_batch, disc_train_type=disc_train_type, logger=self.loggers['logger_debug'], num_batches=num_batches
                )
                d_loss.backward()
            self.optimizer.step('discriminator', scaler=scaler)

            # add the two discriminator's output statistics to the epoch's network outputs statistics list
            if stats:
                # extracting each nested dictionary from the dictionary of dictionaries (stats) and adding to list
                nwks_out_stats += [{k: v_dict} for k, v_dict in stats.items()]
                stats = {}  # reset the stats var

            # check the gradients only for the last batch (to save training time)
            if train_steps_per_epoch == num_batches:
                # get gradients of discriminator after backprop on random reference
                # loop through all gradients of all parameters of model
                for i, param_mx in enumerate(self.model.discriminator.parameters(), 1):
                    try:
                        if torch.isnan(param_mx.grad).any():
                            self.loggers['logger_debug'].error(f"Ep{self.epochs}bch{train_steps_per_epoch}: Random reference training: discriminator layer {i} has at least 1 gradient=NaN.")
                    except TypeError:
                        # no grad tensor available for source classifier if use_adv_cls is False
                        pass

            # train the discriminator (by target reference)
            disc_train_type = "trgref"

            self.optimizer.zero_grad()
            if scaler is not None:
                with torch.cuda.amp.autocast():
                    d_loss, d_losses_ref, stats = compute_d_loss(self.model, self.args.d_loss, x_real, y_org, y_trg, x_ref=x_ref, use_adv_cls=use_adv_cls, use_con_reg=use_con_reg,
                        epoch_num=self.epochs, batch_num=train_steps_per_epoch, debug_batch=debug_batch, disc_train_type=disc_train_type, logger=self.loggers['logger_debug'], num_batches=num_batches
                    )
                scaler.scale(d_loss).backward()
            else:
                d_loss, d_losses_ref, stats = compute_d_loss(self.model, self.args.d_loss, x_real, y_org, y_trg, x_ref=x_ref, use_adv_cls=use_adv_cls, use_con_reg=use_con_reg,
                    epoch_num=self.epochs, batch_num=train_steps_per_epoch, debug_batch=debug_batch, disc_train_type=disc_train_type, logger=self.loggers['logger_debug'], num_batches=num_batches
                )
                d_loss.backward()
            self.optimizer.step('discriminator', scaler=scaler)

            # add the two discriminator's statistics to the epoch's output using the last batch
            if stats:
                # extracting each nested dictionary from the dictionary of dictionaries (stats) and adding to list
                nwks_out_stats += [{k: v_dict} for k, v_dict in stats.items()]
                stats = {}  # reset the stats var

            # check the gradients only for the last batch (to save training time)
            if train_steps_per_epoch == num_batches:
                # get gradients of discriminator after backprop on target reference
                # loop through all gradients of all parameters of model
                for i, param_mx in enumerate(self.model.discriminator.parameters(), 1):
                    try:
                        if torch.isnan(param_mx.grad).any():
                            self.loggers['logger_debug'].error(f"Ep{self.epochs}bch{train_steps_per_epoch}: Target reference training: discriminator layer {i} has at least 1 gradient=NaN.")
                    except TypeError:
                        # no grad tensor available for source classifier if use_adv_cls is False
                        pass
            
            # generator training

            # train the generator (by random reference)
            generator_training_type = "randref"

            self.optimizer.zero_grad()
            if scaler is not None:
                with torch.cuda.amp.autocast():
                    g_loss, g_losses_latent, stats = compute_g_loss(
                        self.model, self.args.g_loss, x_real, y_org, y_trg, z_trgs=[z_trg, z_trg2], use_adv_cls=use_adv_cls,
                        epoch_num=self.epochs, batch_num=train_steps_per_epoch, debug_batch=debug_batch, gen_train_type=generator_training_type,
                        logger=self.loggers['logger_debug'], num_batches=num_batches
                    )
                scaler.scale(g_loss).backward()
            else:
                g_loss, g_losses_latent, stats = compute_g_loss(
                    self.model, self.args.g_loss, x_real, y_org, y_trg, z_trgs=[z_trg, z_trg2], use_adv_cls=use_adv_cls,
                        epoch_num=self.epochs, batch_num=train_steps_per_epoch, debug_batch=debug_batch, gen_train_type=generator_training_type,
                        logger=self.loggers['logger_debug'], num_batches=num_batches
                )
                g_loss.backward()

            self.optimizer.step('generator', scaler=scaler)
            self.optimizer.step('mapping_network', scaler=scaler)
            #self.optimizer.step('style_encoder', scaler=scaler)

            # add the generator's statistics to the epoch's output using the last batch
            if stats:
                # extracting each nested dictionary from the dictionary of dictionaries (stats) and adding to list
                nwks_out_stats += [{k: v_dict} for k, v_dict in stats.items()]
                stats = {}  # reset the stats var

            # check gradients only for the last batch (to save training time)
            if train_steps_per_epoch == num_batches:
                # loop through all gradients of all parameters for each model
                # get gradients of generator after backprop on random reference
                for i, param_mx in enumerate(self.model.generator.parameters(), 1):
                    if torch.isnan(param_mx.grad).any():
                        self.loggers['logger_debug'].error(f"Ep{self.epochs}bch{train_steps_per_epoch}: Random reference training: generator layer {i} has at least 1 gradient=NaN.")
                # get gradients of mapping network
                for i, param_mx in enumerate(self.model.mapping_network.parameters(), 1):
                    if torch.isnan(param_mx.grad).any():
                        self.loggers['logger_debug'].error(f"Ep{self.epochs}bch{train_steps_per_epoch}: Random reference training: mapping network layer {i} has at least 1 gradient=NaN.")
                # get gradients of style encoder
                for i, param_mx in enumerate(self.model.style_encoder.parameters(), 1):
                    if torch.isnan(param_mx.grad).any():
                        self.loggers['logger_debug'].error(f"Ep{self.epochs}bch{train_steps_per_epoch}: Random reference training: style encoder layer {i} has at least 1 gradient=NaN.")

            # train the generator (by target reference)
            generator_training_type = "trgref"

            self.optimizer.zero_grad()
            if scaler is not None:
                with torch.cuda.amp.autocast():
                    g_loss, g_losses_ref, stats = compute_g_loss(
                        self.model, self.args.g_loss, x_real, y_org, y_trg, x_refs=[x_ref, x_ref2], use_adv_cls=use_adv_cls,
                        epoch_num=self.epochs, batch_num=train_steps_per_epoch, debug_batch=debug_batch, gen_train_type=generator_training_type,
                        logger=self.loggers['logger_debug'], num_batches=num_batches
                    )
                scaler.scale(g_loss).backward()
            else:
                g_loss, g_losses_ref, stats = compute_g_loss(
                    self.model, self.args.g_loss, x_real, y_org, y_trg, x_refs=[x_ref, x_ref2], use_adv_cls=use_adv_cls,
                        epoch_num=self.epochs, batch_num=train_steps_per_epoch, debug_batch=debug_batch, gen_train_type=generator_training_type,
                        logger=self.loggers['logger_debug'], num_batches=num_batches
                )
                g_loss.backward()
            self.optimizer.step('generator', scaler=scaler)
            self.optimizer.step('style_encoder', scaler=scaler)

            # add the generator's statistics to the epoch's output using the last batch
            if stats:
                # extracting each nested dictionary from the dictionary of dictionaries (stats) and adding to list
                nwks_out_stats += [{k: v_dict} for k, v_dict in stats.items()]
                stats = {}  # reset the stats var

            # check gradients only for the last batch (to save training time)
            if train_steps_per_epoch == num_batches:
                # get gradients of generator after backprop on target reference
                a = list(self.model.generator.parameters())
                for i, param_mx in enumerate(self.model.generator.parameters(), 1):
                    if torch.isnan(param_mx.grad).any():
                        self.loggers['logger_debug'].error(f"Ep{self.epochs}bch{train_steps_per_epoch}: Target reference training: generator layer {i} has at least 1 gradient=NaN.")

            # TODO: will this be useful info to plot for debugging training?
            # compute moving average of network parameters
            self.moving_average(self.model.generator, self.model_ema.generator, beta=0.999)
            self.moving_average(self.model.mapping_network, self.model_ema.mapping_network, beta=0.999)
            self.moving_average(self.model.style_encoder, self.model_ema.style_encoder, beta=0.999)
            self.optimizer.scheduler()

            for key in d_losses_latent:
                train_losses["train/%s" % key].append(d_losses_latent[key])
            for key in g_losses_latent:
                train_losses["train/%s" % key].append(g_losses_latent[key])
            
            # TODO: remove this, for debugging purposes only!
            # cuts off epoch early after 5 batches
            # if train_steps_per_epoch == 5:
            #    break
        
        # get learning rate of the generator scheduler. Value is the same for all other networks' learning rate schedulers.
        g_lr = self.optimizer.schedulers['generator'].get_last_lr()[0]
        # get average loss values across batches for the epoch's average loss vals
        train_losses = {key: np.mean(value) for key, value in train_losses.items()}
        return train_losses, nwks_out_stats, g_lr

    @torch.no_grad()
    def _eval_epoch(self):
        use_adv_cls = (self.epochs >= self.args.adv_cls_epoch)
        
        eval_losses = defaultdict(list)
        eval_images = defaultdict(list)
        _ = [self.model[k].eval() for k in self.model]
        for eval_steps_per_epoch, batch in enumerate(tqdm(self.val_dataloader, desc="[eval]"), 1):

            ### load data
            batch = [b.to(self.device) for b in batch]
            x_real, y_org, x_ref, x_ref2, y_trg, z_trg, z_trg2 = batch

            # train the discriminator
            d_loss, d_losses_latent, _ = compute_d_loss(
                self.model, self.args.d_loss, x_real, y_org, y_trg, z_trg=z_trg, use_r1_reg=False, use_adv_cls=use_adv_cls)
            d_loss, d_losses_ref, _ = compute_d_loss(
                self.model, self.args.d_loss, x_real, y_org, y_trg, x_ref=x_ref, use_r1_reg=False, use_adv_cls=use_adv_cls)

            # train the generator
            g_loss, g_losses_latent, _ = compute_g_loss(
                self.model, self.args.g_loss, x_real, y_org, y_trg, z_trgs=[z_trg, z_trg2], use_adv_cls=use_adv_cls)
            g_loss, g_losses_ref, _ = compute_g_loss(
                self.model, self.args.g_loss, x_real, y_org, y_trg, x_refs=[x_ref, x_ref2], use_adv_cls=use_adv_cls)

            for key in d_losses_latent:
                eval_losses["eval/%s" % key].append(d_losses_latent[key])
            for key in g_losses_latent:
                eval_losses["eval/%s" % key].append(g_losses_latent[key])
            
            # TODO: remove this, for debugging purposes only!
            # cuts off epoch early after 5 batches
            # if eval_steps_per_epoch == 5:
            #    break

#             if eval_steps_per_epoch % 10 == 0:
#                 # generate x_fake
#                 s_trg = self.model_ema.style_encoder(x_ref, y_trg)
#                 F0 = self.model.f0_model.get_feature_GAN(x_real)
#                 x_fake = self.model_ema.generator(x_real, s_trg, masks=None, F0=F0)
#                 # generate x_recon
#                 s_real = self.model_ema.style_encoder(x_real, y_org)
#                 F0_fake = self.model.f0_model.get_feature_GAN(x_fake)
#                 x_recon = self.model_ema.generator(x_fake, s_real, masks=None, F0=F0_fake)
                
#                 eval_images['eval/image'].append(
#                     ([x_real[0, 0].cpu().numpy(),
#                     x_fake[0, 0].cpu().numpy(),
#                     x_recon[0, 0].cpu().numpy()]))

        eval_losses = {key: np.mean(value) for key, value in eval_losses.items()}
        eval_losses.update(eval_images)
        return eval_losses
