# This script only assumes one speaker example wav file per speaker in the inference folder.
# Only used for Demo purposes of converting one example file from source speaker to different target speakers.

# Source: http://speech.ee.ntu.edu.tw/~jjery2243542/resource/model/is18/en_speaker_used.txt
# Source: https://github.com/jjery2243542/voice_conversion

# load packages
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import random
import argparse
from munch import Munch
import yaml
import numpy as np
import torch
import torchaudio
import librosa
import soundfile as sf


from models import Generator, MappingNetwork, StyleEncoder
from parallel_wavegan.utils import load_model
from Utils.JDC.model import JDCNet

import time



to_mel = torchaudio.transforms.MelSpectrogram(
    n_mels=80, n_fft=2048, win_length=1200, hop_length=300)
mean, std = -4, 4


def preprocess(wave):
    # convert a .wav time-domain numpy array audio sample to a mel-spec tensor
    wave_tensor = torch.from_numpy(wave).float()
    mel_tensor = to_mel(wave_tensor)
    mel_tensor = (torch.log(1e-5 + mel_tensor.unsqueeze(0)) - mean) / std
    return mel_tensor


def build_model(model_params={}):
    args = Munch(model_params)
    generator = Generator(args.dim_in, args.style_dim, args.max_conv_dim, w_hpf=args.w_hpf, F0_channel=args.F0_channel)
    mapping_network = MappingNetwork(args.latent_dim, args.style_dim, args.num_domains, hidden_dim=args.max_conv_dim)
    style_encoder = StyleEncoder(args.dim_in, args.style_dim, args.num_domains, args.max_conv_dim)
    
    nets_ema = Munch(generator=generator,
                     mapping_network=mapping_network,
                     style_encoder=style_encoder)

    return nets_ema


def compute_style(speakers_dict, model):
    # compute latent space style encoding using either style encoder to 
    # convert reference audio if explicitly given,
    # or randomly sample encoding using mapping network from latent space learned by the network.
    
    # populate dict where key=speakerID, val= tuple
    # tuple = (style embedding vector of reference audio sample from speaker, index of speaker in speakers list)
    reference_embeddings = {}
    # loop over entries in speakers dict
    # key = speakerID, path = path/to/audio, speaker = index of speaker in speakers list
    for speaker_name, (path, speaker_id) in speakers_dict.items():
        # no reference audio
        if path == "":
            label = torch.LongTensor([speaker_id]).to('cuda')
            latent_dim = model.mapping_network.shared[0].in_features
            # mapping network takes speakerID as input to sample random latent style from that speaker's domain
            ref = model.mapping_network(torch.randn(1, latent_dim).to('cuda'), label)
        else:
            # load the reference audio sample
            wave, sr = librosa.load(path, sr=24000)
            audio, index = librosa.effects.trim(wave, top_db=30)
            if sr != 24000:
                # resample audio to 24kHz sampling rate
                audio = librosa.resample(audio, sr, 24000)
            # convert wav to mel-spec
            mel_tensor = preprocess(audio).to('cuda')

            # get style embedding vector of ref audio sample from the speaker encoder
            with torch.no_grad():
                # additional label input to style encoder is speaker index from speaker list, used for organising batches,
                #  not for actual vector computing.
                # main input to style encoder is the mel-spec of the audio sample (referred to as the reference audio)
                label = torch.LongTensor([speaker_id])
                ref = model.style_encoder(mel_tensor.unsqueeze(1), label)
        reference_embeddings[speaker_name] = (ref, label)
    
    return reference_embeddings


def main():
    # make subdir in model folder for inference results of specified model.
    out_dir = args.model_folder_path + args.model_filename.split('.pth')[0] + '_demo_output_' + args.suffix_tag
    if not os.path.exists(out_dir): os.makedirs(out_dir, exist_ok=True)

    # load trained starganv2-vc model to GPU device
    #model_path = 'Models/adult/' + 'epoch_00050.pth'
    model_path = args.model_folder_path + args.model_filename
    config_path = args.model_folder_path + 'config.yml'
    with open(config_path) as f:
        starganv2_config = yaml.safe_load(f)
    starganv2 = build_model(model_params=starganv2_config["model_params"])
    params = torch.load(model_path, map_location='cpu')
    params = params['model_ema']
    _ = [starganv2[key].load_state_dict(params[key]) for key in starganv2]
    _ = [starganv2[key].eval() for key in starganv2]
    starganv2.style_encoder = starganv2.style_encoder.to('cuda')
    starganv2.mapping_network = starganv2.mapping_network.to('cuda')
    starganv2.generator = starganv2.generator.to('cuda')

    # load F0 model to GPU device
    F0_model = JDCNet(num_class=1, seq_len=192)
    params = torch.load("Utils/JDC/bst.t7")['net']
    F0_model.load_state_dict(params)
    _ = F0_model.eval()
    F0_model = F0_model.to('cuda')
    # load vocoder to GPU device
    vocoder = load_model("Vocoder/checkpoint-400000steps.pkl").to('cuda').eval()
    vocoder.remove_weight_norm()
    _ = vocoder.eval()
    

    # Global list of all speaker IDs used in training this model.
    # Note: this is just an int list from 1 to n, where n = the number of speakers in the training + validation dataset.
    # This information can be generated by calling Data/create_speaker_names_ids_file.py, which will generate
    #  a text file listing the IDs of all the speakers in the dataset used to train the model.
    speakers = range(1, args.num_train_spkrs+1)

    # List of speaker IDs used in inference.
    # Get the speaker IDs of the speakers you will use in inference from the training dataset folder's speaker_ids.txt file.
    # get all speakers from the Demo folder, create list manually
    # the Demo folder's speaker's audio files are used as source and/or reference audio for VC
    # one audio file per speaker in Demo folder (Note: it is not required that the text spoken is identical for all speakers.)
    selected_speakers = [1, 2, 3, 4]  # list of speakers selected for all-to-all voice conversion

    # choose a speaker's audio sample as the source audio that will be voice converted using different reference speakers styles
    # NOTE: only assumes 1 wav file in the inference speaker folder, so will index 0
    wav_path = [os.path.join(args.infer_folder_path, args.source_speaker_name, f) for f in os.listdir(os.path.join(args.infer_folder_path, args.source_speaker_name)) if os.path.isfile(os.path.join(args.infer_folder_path, args.source_speaker_name, f)) and f.endswith(".wav")][0]

    # resample audio to 24kHz sampling rate
    h, source_sr = librosa.load(wav_path, sr=24000)
    src_audio = h / np.max(np.abs(h))
    src_audio.dtype = np.float32
    # show the audio sample
    print(f"Original source audio from {args.source_speaker_name} to convert:")

    sf.write(f"{out_dir}/orig_src_spkr_{args.source_speaker_name}.wav", src_audio, source_sr)
    print(f"saved as {out_dir}/orig_src_spkr_{args.source_speaker_name}.wav")

    # reconstruction using vocoder of original source audio
    print('Reconstructed original source audio using vocoder for speaker %s:' % args.source_speaker_name)

    mel = preprocess(h)
    c = mel.transpose(-1, -2).squeeze().to('cuda')
    with torch.no_grad():
        recon = vocoder.inference(c)
        recon = recon.view(-1).cpu().numpy()

    sf.write(f"{out_dir}/orig_src_spkr_{args.source_speaker_name}_vocoded.wav", recon, source_sr)
    print(f"saved as {out_dir}/orig_src_spkr_{args.source_speaker_name}_vocoded.wav")

    # ------------------- with reference, using style encoder ------------------------------------

    # 'style' reference doesn't mean style transfer in this project.
    # style reference audio specifies what the target voice should sound like,
    # while source audio contains the linguistic content to say in the target voice
    # therefore, voice conversion = saying source audio content in reference audio voice

    # populate dict from speaker_ids.txt used in training where key=speakerName, val=ID
    train_spkr_ids_dict = {}

    # load the speaker IDs for each training speaker from the speaker_ids.txt file used during training
    with open(args.train_spkr_ids_path, 'r') as f:
        for line in f:
            spkr_name, id = line.split('|')[0], int(line.split('|')[1])
            train_spkr_ids_dict[spkr_name] = id

    # populate dict where key=speakerName, val=tuple, where:
    # tuple = ("path/to/Demo/speaker/audio/sample.wav", speaker ID)
    # loop through all speakers in the Demo folder
    speakers_dict = {}

    for dirpath, subdirs, _ in os.walk(args.infer_folder_path, topdown=True):
        for subdir in subdirs:
            # subdir is name of speaker
            if subdir in train_spkr_ids_dict:
                # again assumes only 1 wav file in dir, so index taken is always 0
                speakers_dict[subdir] = ([os.path.join(dirpath, subdir, f) for f in os.listdir(os.path.join(dirpath, subdir)) if os.path.isfile(os.path.join(dirpath, subdir, f)) and f.endswith(".wav")][0],
                                          train_spkr_ids_dict[subdir])

    # get the latent space style embeddings for each speaker's reference audio sample from the style encoder
    reference_embeddings = compute_style(speakers_dict, starganv2)

    start = time.time()
    
    source = preprocess(src_audio).to('cuda:0')
    keys = []
    converted_samples = {}
    reconstructed_samples = {}
    converted_mels = {}

    # loop over style reference embeddings, where key = speakerID of ref style emb, ref = encoded mel-spec of style audio reference for speakerID
    for key, (ref, _) in reference_embeddings.items():
        with torch.no_grad():
            # extract the fundamental frequency from source input mel-spec
            f0_feat = F0_model.get_feature_GAN(source.unsqueeze(1))
            # get output voice converted mel-spec from StarGANv2-VC generator's Decoder
            #  after passing source mel-spec, ref mel-spec and F0 of source mel-spec to Encoder
            out = starganv2.generator(source.unsqueeze(1), ref, F0=f0_feat)
            
            # synthesise output mel-spec using vocoder to wav
            c = out.transpose(-1, -2).squeeze().to('cuda')
            y_out = vocoder.inference(c)
            y_out = y_out.view(-1).cpu()

            if key not in speakers_dict or speakers_dict[key][0] == "":
                recon = None
            else:
                # get orig ref audio
                wave, sr = librosa.load(speakers_dict[key][0], sr=source_sr)
                # get mel-spec version of ref audio
                mel = preprocess(wave)
                c = mel.transpose(-1, -2).squeeze().to('cuda')
                # synthesise back reconstructed version using vocoder
                recon = vocoder.inference(c)
                recon = recon.view(-1).cpu().numpy()

        # add voice converted source wav that used ref speakerID as style emb
        converted_samples[key] = y_out.numpy()
        # add vocoder reconstructed wav of original ref wav for ref speakerID
        reconstructed_samples[key] = recon
        # non-synthesised mel-spec output of vocoder after voice conversion
        converted_mels[key] = out
        # add speakerID to list of ref speakers used for style
        keys.append(key)
        
    end = time.time()
    print('total processing time: %.3f sec' % (end - start) )

    for key, wave in converted_samples.items():
        print("-----------------------------------------------------")
        print('Converted source audio conditioned on speaker %s as the style ref:' % key)

        sf.write(f"{out_dir}/src_conditioned_on_{key}_vocoded.wav", wave, source_sr)
        print(f"saved as {out_dir}/src_conditioned_on_{key}_vocoded.wav")

        print('Reference audio of speaker %s used for style conditioning above:' % key)
        # again assumes only 1 wav file in dir, so index taken is always 0
        wav_path_ref = [os.path.join(args.infer_folder_path, key, f) for f in os.listdir(os.path.join(args.infer_folder_path, key)) if os.path.isfile(os.path.join(args.infer_folder_path, key, f)) and f.endswith(".wav")][0]

        # resample audio to 24kHz sampling rate (same as during inference)
        audio, source_sr = librosa.load(wav_path_ref, sr=24000)
        audio = audio / np.max(np.abs(audio))
        audio.dtype = np.float32

        sf.write(f"{out_dir}/conditioning_audio_{key}.wav", audio, source_sr)
        print(f"saved as {out_dir}/conditioning_audio_{key}.wav")

    # -------------------------- no reference audio, using mapping network ---------------------------

    # style encoder not used as there is no user-specified reference audio
    # mapping network samples a random latent style embedding from the style encoder's latent output space
    # the speakerID is inputted to the mapping network to sample a random style embedding from that ref speaker's domain

    for k in speakers_dict:
        # path to audio is empty, meaning there is no reference style waveform
        speakers_dict[k] = ('', speakers_dict[k][1])

    # randomly sampled reference style embeddings
    reference_embeddings = compute_style(speakers_dict, starganv2)

    # conversion 
    start = time.time()
        
    source = preprocess(src_audio).to('cuda:0')
    keys = []
    converted_samples = {}
    reconstructed_samples = {}
    converted_mels = {}

    # loop over style reference embeddings, where key = speakerID of ref style emb, ref = randomly sampled style emb
    for key, (ref, _) in reference_embeddings.items():
        with torch.no_grad():
            # extract the fundamental frequency from source input mel-spec
            f0_feat = F0_model.get_feature_GAN(source.unsqueeze(1))
            # get output voice converted mel-spec from StarGANv2-VC generator conditioned on the randomly sampled style embedding
            out = starganv2.generator(source.unsqueeze(1), ref, F0=f0_feat)
            
            # synthesise output mel-spec using vocoder to wav
            c = out.transpose(-1, -2).squeeze().to('cuda')
            y_out = vocoder.inference(c)
            y_out = y_out.view(-1).cpu()

            if key not in speakers_dict or speakers_dict[key][0] == "":
                recon = None
            else:
                wave, sr = librosa.load(speakers_dict[key][0], sr=24000)
                mel = preprocess(wave)
                c = mel.transpose(-1, -2).squeeze().to('cuda')
                recon = vocoder.inference(c)
                recon = recon.view(-1).cpu().numpy()

        # add voice converted wav for ref speakerID
        converted_samples[key] = y_out.numpy()
        # populated with val = None since no reference audio was specified
        reconstructed_samples[key] = recon
        # non-synthesised mel-spec output of vocoder after voice conversion
        converted_mels[key] = out
        # add speakerID to list
        keys.append(key)

    end = time.time()
    print('total processing time: %.3f sec' % (end - start) )

    for key, wave in converted_samples.items():
        print("-----------------------------------------------------")
        print('Converted source audio using %s for latent sampling:' % key)
        
        sf.write(f"{out_dir}/src_conditioned_rand_latent_{key}_vocoded.wav", wave, source_sr)
        print(f"saved as {out_dir}/src_conditioned_rand_latent_{key}_vocoded.wav")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run demo source-to-all voice conversion inference on a saved model checkpoint and save the results into a new subfolder in the 'model_folder_path' directory.")
    parser.add_argument("--model_folder_path", type=str, default=None, required=True,
                        help="Path to the folder containing saved model checkpoints, where the new results subfolder will be created.")
    parser.add_argument("--suffix_tag", type=str, default='', required=False,
                    help="New subfolder's suffix ending, useful when doing multiple runs on the same model checkpoint.")
    parser.add_argument("--model_filename", type=str, default=None, required=True,
                        help="Name of the saved model file to load with the .pth extension")
    parser.add_argument("--infer_folder_path", type=str, default=None, required=True,
                    help="Path to the folder containing speaker folders used for Demo inference. NOTE: script assumes there is only one audio file in each speaker folder. The folder names should also match speaker folder names from the training dataset used by this model.")
    parser.add_argument("--num_train_spkrs", type=int, default=None, required=True,
                        help="The number of speakers used to train the model. Should be equal to model_params.num_domains from the model's config yaml file (also is equal to the number of speaker folders in the training dataset used).")
    parser.add_argument("--source_speaker_name", type=str, default=None, required=True,
                        help="The name of the source speaker to voice convert. It should be one of the speakers from infer_folder_path.")
    parser.add_argument("--train_spkr_ids_path", type=str, default=None, required=True,
                    help="Path to file containing the speaker IDs for each speaker name from the training dataset of the model. Such a file is called speaker_ids.txt and is located in the training dataset folder, created by Data/create_train_test_split.py or Data/create_speaker_names_ids_file.py")

    # parse program arguments
    global args
    args = parser.parse_args()

    main()